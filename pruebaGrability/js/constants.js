angular.module('app.constantes', [])
  .constant('constantes', {
      "conexion" : {
        "headers" : {
          'Access-Control-Allow-Origin' : '*',
           'Access-Control-Allow-Methods' : 'POST, GET, OPTIONS, PUT',
           'Content-Type': 'application/json',
           'Accept': 'application/json'
         },
         "ENDPOINT" : 'data/news_mock.json',
         "method" : 'get',
      }
  });
