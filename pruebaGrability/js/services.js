angular.module('app.services', [])
  .service('servicioConsulta', function($http , $q , constantes) {
    var deferred = $q.defer();
    var promise = deferred.promise;
    return  {
      consultarURL : function(URL){
        $http({
        method:  constantes.conexion.method,
        url: URL,
        headers: constantes.conexion.headers
        }).success(function (response , status , headers ,config ,statusText  ) {
          var respuesta = {
              response : response,
              status	: status,
              headers	: headers,
              config	: statusText
          };
            deferred.resolve(respuesta);
            return response;
        }).error(function(response , status , headers ,config ,statusText) {

          var respuesta = {
              response : response,
              status	: status,
              headers	: headers,
              config	: statusText
          };
            deferred.reject(respuesta);
            return response;
        });

        promise.success = function(fn) {
            promise.then(fn);
            return promise;
        }
        promise.error = function(fn) {
            promise.then(null, fn);
            return promise;
        }
        return promise;
      }
    }
  });
