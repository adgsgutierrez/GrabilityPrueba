angular.module('app.controllers', [ 'ngAnimate' ])
  .controller('informacionAplicacion', function($scope , constantes , servicioConsulta) {

    console.log(constantes);
    $scope.data = {
      filtro : '',
      url : '' + constantes.conexion.ENDPOINT,
      consultando : false,
    };
    $scope.informacion = [];

    $scope.consultarUrl = function(){
      //console.log(" URL "+$scope.data.url);
      servicioConsulta.consultarURL($scope.data.url)
      .success(function(response){
        //console.log("Termino la consulta "+JSON.stringify(response.response));
        $scope.informacion = '';
        $scope.informacion = response.response;
      }).error(function(){
        console.log("Error en la consulta "+JSON.stringify(response));
      });
    };
    var dialog = document.querySelector('dialog');
    if (! dialog.showModal) {
      dialogPolyfill.registerDialog(dialog);
    }

    $scope.acerca = function(){
        dialog.showModal();
    };

    dialog.querySelector('.close').addEventListener('click', function() {
      dialog.close();
    });
  });
