Este proyecto se crea con el fin de aplicar para la oferta de la empresa 
Grability como Desarrollador Front End

Es indispensable tener un servidor de node JS disponible para ejecutar la
aplicación de forma optima

Paso 1:
El servidor recomendado es https://www.npmjs.com/package/http-server

Paso 2:
Clonar el proyecto 
git clone https://gitlab.com/adgsgutierrez/GrabilityPrueba.git

Paso 3:
dirigirse a la ruta PATH[direccion donde se clono]/pruebaGrability

Paso 4:
Dentro de esta direccion ejecutar el comando 
http-serve [Esto si instalaron el servidor http-server de node JS]

Desarrollado por Aric Gutierrez

Herramientas utilizadas

Diseño, Interfaz y animaciones
Bootstrap         : https://getmdl.io/components/index.html [Febrero/2016]
AngularJs 1.5.0   : https://angularjs.org
